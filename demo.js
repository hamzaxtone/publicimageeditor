$(function(){

 
// Do some initializing stuff
fabric.Object.prototype.set({
    transparentCorners: true,
    cornerColor: 'rgba(102,153,255,0.5)',
    cornerSize: 12,
    padding: 5
});

 
var canvas = this.__canvas = new fabric.Canvas('c',{ selection: false , isDrawingMode : false});
var line, isDown, startPosition={}, rect,drawingMode=true;
var textrec;
var textcircle;
var circle;
var canvasScale = 1;
var SCALE_FACTOR = 1.3;


var linetext;

var $ = function(id){return document.getElementById(id)};
  var drawingModeEl = $('drawing-mode');

var iscircle = false;
var isline = false;
var isrectange = false;
var istext =false;
// http://bedsidexray.com/wp-content/uploads/2014/10/Chest-X-Ray-Image.jpg
canvas.setBackgroundImage('http://bedsidexray.com/wp-content/uploads/2014/10/Chest-X-Ray-Image.jpg', canvas.renderAll.bind(canvas), {
  
    backgroundImageStretch: false
});






$('textt').onclick = function() {
    
    istext = this.checked;

    iscircle = false;
    isline = false;
    isrectange = false;
    canvas.isDrawingMode=false;
    canvas.selection= false;
  };

$('delete').onclick = function() {
    
deleteObjects();
    iscircle = false;
    istext = false;
    isline = false;
    isrectange = false;
    canvas.isDrawingMode=false;
    canvas.selection= true;
  };

$('rectange').onclick = function() {
    
    isrectange = this.checked;
    istext = false;
    iscircle = false;
    isline = false;
    canvas.isDrawingMode=false;
    canvas.selection= false;
  };
   
$('circle').onclick = function() {
    

    iscircle = this.checked;
    istext = false;
    isline = false;
    isrectange = false;
    canvas.isDrawingMode=false;
    canvas.selection= false;
  };
$('line').onclick = function() {
    

    isline = this.checked;
    istext = false;
    iscircle = false;
    isrectange = false;
    canvas.isDrawingMode=false;
    canvas.selection= false;
  };

  $('zoominn').onclick = function() {
zoomIn();
};

  $('Reset').onclick = function() {
canvasreset();
};

function canvasreset() {
    canvas.setHeight(canvas.getHeight() * (1 / canvasScale));
    canvas.setWidth(canvas.getWidth() * (1 / canvasScale));
canvas.renderAll();
canvasScale = 1;
}

// Zoom In
function zoomIn() {
    canvasScale = canvasScale * SCALE_FACTOR;
    canvas.setHeight(canvas.getHeight() * SCALE_FACTOR);
    canvas.setWidth(canvas.getWidth() * SCALE_FACTOR);
    canvas.setZoom(canvasScale)
    canvas.renderAll();
}

 $('zoomoutt').onclick = function() {
zoomOut()
};
// Zoom Out
function zoomOut() {
    canvasScale = canvasScale / SCALE_FACTOR;
    canvas.setHeight(canvas.getHeight() * (1 / SCALE_FACTOR));
    canvas.setWidth(canvas.getWidth() * (1 / SCALE_FACTOR));
    canvas.setZoom(canvasScale)
    canvas.renderAll();
};



  drawingModeEl.onclick = function() {
    iscircle = false;
    isline = false;
    istext = false;
    isrectange = false;
    canvas.isDrawingMode = !canvas.isDrawingMode;
    if (canvas.isDrawingMode) {
      drawingModeEl.innerHTML = 'Cancel drawing mode';
      drawingOptionsEl.style.display = '';
    }
    else {
      drawingModeEl.innerHTML = 'Enter drawing mode';
      drawingOptionsEl.style.display = 'none';
    }
  };

canvas.on('mouse:down', function(event){
 if (!drawingMode) return;
    isDown = true;
    //console.log(event.e.clientX,event.e.clientY);

    startPosition.x=event.e.clientX;
    startPosition.y=event.e.clientY;


    var points = [startPosition.x, startPosition.y, startPosition.x, startPosition.y];

if (istext) {
    var fabicText = new fabric.IText('Click to change Text', {
        left: event.e.clientX,
        top: event.e.clientY
    });
    canvas.add(fabicText);
    fabicText.set({ fill: 'blue' });
    canvas.setActiveObject(fabicText);
    fabicText.enterEditing()
    fabicText.hiddenTextarea.focus();

// var text = new fabric.Text('hello world', { left: event.e.clientX, top: event.e.clientY });
// canvas.add(text);    
}


    if(iscircle)
    {

          circle = new fabric.Circle({
            radius:"0",
            left:event.e.clientX,
            top:event.e.clientY,
            lockMovementX :"true",
            lockMovementY :"true",
            originX:event.e.clientX,
            originY:event.e.clientY,
            stroke:'red',
            strokeWidth:1,
            fill:''
        });

   textcircle = new fabric.Text('width = '+event.e.clientX+'height = '+event.e.clientY, {
            left:event.e.clientX-10,
            top:event.e.clientY-10,
            lockMovementX :"false",
            lockMovementY :"false",
            fontSize: 20,

        
          });


 canvas.add(textcircle);
 canvas.add(circle);

    }

    //console.log(startPosition);

    if (isrectange) {
        rect = new fabric.Rect({
            left:event.e.clientX,
            top:event.e.clientY,
            lockMovementX :"false",
            lockMovementY :"false",
            
            stroke:'red',
            strokeWidth:1,
            fill:''
        });
         textrec = new fabric.Text('width = '+event.e.clientX+'height = '+event.e.clientY, {
            left:event.e.clientX-10,
            top:event.e.clientY-10,
            lockMovementX :"false",
            lockMovementY :"false",
            fontSize: 14
          
          });
          
       canvas.add(textrec);
        canvas.add(rect);
       
  
    }


              
        /*circle = new fabric.Circle({
            radius:"0",
            left:event.e.clientX,
            top:event.e.clientY,
            originX:event.e.clientX,
            originY:event.e.clientY,
            stroke:'red',
            strokeWidth:2,
            fill:''
        });*/
        

if (isline) {
     line = new fabric.Line(points, {
                         strokeWidth: 2,
                         fill: 'red',
                         stroke: 'red',
                         originX: 'center',
                         originY: 'center'
                     });
                     canvas.add(line);
       
}
        
        
          
          
          
        //canvas.add(textcircle);
       // canvas.add(circle);
        
});

canvas.on('mouse:move', function(event){
    if (!isDown || !drawingMode) return;
    if (isrectange) {
     rect.setWidth(Math.abs( event.e.clientX-startPosition.x ));
     rect.setHeight(Math.abs( event.e.clientY -startPosition.y ));
     
    textrec.setText('width = '+event.e.clientX+'height = '+event.e.clientY);
        
    }
     
   



  if(iscircle){

     textcircle.setText('radius = '+Math.abs( event.e.clientX-startPosition.x));
     
     circle.setRadius(Math.abs( event.e.clientX-startPosition.x ));

    }


if (isline) {
     line.set({ x2: event.e.clientX, y2: event.e.clientY });

     calculate(event.e.clientX,event.e.clientY);
    
}

                     


    canvas.renderAll();
});

canvas.on('mouse:up', function(){
    isDown = false;

if (isline) {
    canvas.add(linetext);
    canvas.renderAll();

}

if (isrectange) {
     canvas.add(rect);
}
    
});

canvas.on('object:selected', function(){
    drawingMode = false;         
});

canvas.on('object:selected', function(){
    drawingMode = false;         
});
canvas.on('selection:cleared', function(){  
    drawingMode = true;      
});





             function calculate(endx , endy) {

                     var linePixelsX = startPosition.x - endx;
                     var linePixelsY = startPosition.y - endy;
                     var gregVar = linePixelsX - linePixelsY;
                     // horizontal/vertical components of line in pixels
                     var horizontalMPP = 800 / 500;
                     var verticalMPP = 800 / 500;
                     // horizontal/vertical meters per pixel, p being according canvas measurement in pixels and m being the measurement (in meters) of the area it should represent

                     var lineMetersX = linePixelsX * horizontalMPP;
                     var lineMetersY = linePixelsY * verticalMPP;
                     // calculate horizontal/vertical line components in meters

                     var lineMetersTotal = (lineMetersX * lineMetersX + lineMetersY * lineMetersY);
                     lineMetersTotal = Math.sqrt(lineMetersTotal).toFixed(2);

                     linetext = new fabric.Text('Length ' + lineMetersTotal, { left: endx, top: endy, fontSize: 12 });
                     
                    
                 }
                 // select all objects
function deleteObjects(){
  var activeObject = canvas.getActiveObject(),
    activeGroup = canvas.getActiveGroup();
    if (activeObject) {
        if (confirm('Are you sure?')) {
            canvas.remove(activeObject);
        }
    }
    else if (activeGroup) {
        if (confirm('Are you sure?')) {
            var objectsInGroup = activeGroup.getObjects();
            canvas.discardActiveGroup();
            objectsInGroup.forEach(function(object) {
            canvas.remove(object);
            });
        }
    }
}
function dlCanvas() {
  var dt = canvas.toDataURL('image/png');
  /* Change MIME type to trick the browser to downlaod the file instead of displaying it */
  dt = dt.replace(/^data:image\/[^;]*/, 'data:application/octet-stream');

  /* In addition to <a>'s "download" attribute, you can define HTTP-style headers */
  dt = dt.replace(/^data:application\/octet-stream/, 'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename=Canvas.png');

  this.href = dt;
};
document.getElementById("dl").addEventListener('click', dlCanvas, false);
});

