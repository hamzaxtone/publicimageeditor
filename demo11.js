$(function(){


// Do some initializing stuff
fabric.Object.prototype.set({
    transparentCorners: true,
    cornerColor: 'rgba(102,153,255,0.5)',
    cornerSize: 12,
    padding: 5
});

 
var canvas = this.__canvas = new fabric.Canvas('c',{ selection: false });

var line, isDown, startPosition={}, rect,drawingMode=true;
var textrec;
var textcircle;
var circle;

var linetext;





   



canvas.on('mouse:down', function(event){
 if (!drawingMode) return;
    isDown = true;
    //console.log(event.e.clientX,event.e.clientY);
    startPosition.x=event.e.clientX;
    startPosition.y=event.e.clientY;


    var points = [startPosition.x, startPosition.y, startPosition.x, startPosition.y];

    //console.log(startPosition);

      /*  rect = new fabric.Rect({
            left:event.e.clientX,
            top:event.e.clientY,
            lockMovementX :"false",
            lockMovementY :"false",
            width:0,
            height:0,
            stroke:'red',
            strokeWidth:2,
            fill:''
        });*/
        
        
        /*circle = new fabric.Circle({
            radius:"0",
            left:event.e.clientX,
            top:event.e.clientY,
            originX:event.e.clientX,
            originY:event.e.clientY,
            stroke:'red',
            strokeWidth:2,
            fill:''
        });*/
        


        line = new fabric.Line(points, {
                         strokeWidth: 2,
                         fill: 'red',
                         stroke: 'red',
                         originX: 'center',
                         originY: 'center'
                     });
                     canvas.add(line);
        
        
           /*textrec = new fabric.Text('width = '+event.e.clientX+'height = '+event.e.clientY, {
            left:event.e.clientX-10,
            top:event.e.clientY-10,
            lockMovementX :"false",
            lockMovementY :"false",
            fontSize: 10
          
          });
          */
        /*  textcircle = new fabric.Text('width = '+event.e.clientX+'height = '+event.e.clientY, {
            left:event.e.clientX-10,
            top:event.e.clientY-10,
            lockMovementX :"false",
            lockMovementY :"false",
            fontSize: 10
          
          });*/
          
          
        //canvas.add(textcircle);
        //canvas.add(textrec);
       // canvas.add(circle);
        //canvas.add(rect);
       
});

canvas.on('mouse:move', function(event){
    if (!isDown || !drawingMode) return;

     //rect.setWidth(Math.abs( event.e.clientX-startPosition.x ));
     //rect.setHeight(Math.abs( event.e.clientY -startPosition.y ));
     
    //textrec.setText('width = '+event.e.clientX+'height = '+event.e.clientY);
     
    // textcircle.setText('radius = '+Math.abs( event.e.clientX-startPosition.x));
     
     //circle.setRadius(Math.abs( event.e.clientX-startPosition.x ));


     line.set({ x2: event.e.clientX, y2: event.e.clientY });

     calculate(event.e.clientX,event.e.clientY);
                     


    canvas.renderAll();
});

canvas.on('mouse:up', function(){
    isDown = false;

    canvas.add(linetext);
canvas.renderAll();
     //canvas.add(rect);
});

canvas.on('object:selected', function(){
    drawingMode = false;         
});

canvas.on('object:selected', function(){
    drawingMode = false;         
});
canvas.on('selection:cleared', function(){  
    drawingMode = true;      
});





             function calculate(endx , endy) {

                     var linePixelsX = startPosition.x - endx;
                     var linePixelsY = startPosition.y - endy;
                     var gregVar = linePixelsX - linePixelsY;
                     // horizontal/vertical components of line in pixels
                     var horizontalMPP = 800 / 500;
                     var verticalMPP = 800 / 500;
                     // horizontal/vertical meters per pixel, p being according canvas measurement in pixels and m being the measurement (in meters) of the area it should represent

                     var lineMetersX = linePixelsX * horizontalMPP;
                     var lineMetersY = linePixelsY * verticalMPP;
                     // calculate horizontal/vertical line components in meters

                     var lineMetersTotal = (lineMetersX  lineMetersX + lineMetersY  lineMetersY);
                     lineMetersTotal = Math.sqrt(lineMetersTotal).toFixed(2);

                     linetext = new fabric.Text('Length ' + lineMetersTotal, { left: endx, top: endy, fontSize: 12 });
                     
                    
                 }

});